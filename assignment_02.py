# -*- coding: utf-8 -*-
"""Assignment -02

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1OXZCIIPHt5-6Iv0gBUdFcDCO8aAEdwXA
"""

#3)Create a class Person with instance attributes name and age. Add a class attribute count that keeps track of the number of Person objects created. Create multiple instances of Person and print the value of the count attribute.
class Person:
  count=0
  def __init__(self,name,age):
    self.name=name
    self.age=age
    Person.count+=1

person_1=Person("Pooja",18)
person_2=Person("Roja",39)
person_3=Person("sreedhar",45)
person_4=Person("Likhitha",11)
person_5=Person("Ridhima",19)
print(Person.count,"is the count")

#4)Define a class BankAccount with instance attributes account_number and balance. Add a class attribute interest_rate that is set to 0.02. Add methods deposit and withdraw to modify the balance. Add a class method get_interest_rate that returns the value of the interest_rate attribute.
class BankAccount:
   interest_rate=0.02
   def __init__(self,account_number,balance):
    self.account_number=account_number
    self.balance=balance
   def deposit(self,amount):
    self.balance+=amount
   def withdraw(self,amount):
    self.balance-=amount
   def get_interest_rate(self):
    return BankAccount.interest_rate

account_1=BankAccount(int(input("Account Number: ")),int(input("Balance amount: ")))
account_1.deposit(int(input("Amount to be deposited to the account: ")))
print(account_1.balance)

account_1.withdraw(int(input("Amount to be withdrawed to the account: ")))
print(account_1.balance)

print("Interest rate in the bank is {}".format(account_1.get_interest_rate()))

class Employee:
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary

    def calculate_bonus(self):
        return self.salary * 0.10

class Manager(Employee):
    def calculate_bonus(self):
        return self.salary * 0.20

class Salesperson:
    def __init__(self, name):
        self.name = name
        self.record = {}
        self.value = 0.0
    
    def get_name(self):
        return self.name
    
    def buy_pro(self, product_name, market_price, quantity):
        if product_name in self.record:
            cost, existing_quantity = self.record[product_name]
            self.record[product_name] = [cost, existing_quantity + quantity]
        else:
            self.record[product_name] = [market_price, quantity]
        
        self.value -= market_price * quantity
    
    def sell_pro(self, product_name, market_price, quantity):
        if product_name in self.record:
            cost, existing_quantity = self.record[product_name]
            
            if quantity >= existing_quantity:
                del self.record[product_name]
                self.value += market_price * existing_quantity
            else:
                self.record[product_name] = [cost, existing_quantity - quantity]
                self.value += market_price * quantity
    
    def get_record(self):
        return self.record
    
    def get_value(self):
        return self.value

import random

class Card:
    def __init__(self, suit, value):
        self.suit = suit
        self.value = value
    



class Deck:
    def __init__(self):
        suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
        values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
        self.cards = [Card(suit, value) for suit in suits for value in values]
    
    def shuffle(self):
        random.shuffle(self.cards)
    
    def deal(self):
        if len(self.cards) > 0:
            return self.cards.pop()
        else:
            return None

class BankAccount:
    def __init__(self, initial_balance):
        self.balance = initial_balance
    
    def deposit(self, amount):
        self.balance += amount
        
    
    def withdraw(self, amount):
        if self.balance - amount >= 1000:
            self.balance -= amount
           
        else:
            print("Insufficient balance. Minimum balance of Rs. 1000 must be maintained.")
        

class MinimumBalanceAccount(BankAccount):
    def withdraw(self, amount):
        if self.balance - amount >= 1000:
            super().withdraw(amount)
        else:
            print("Insufficient balance. Minimum balance of Rs. 1000 must be maintained.")

#1 .WRITE A CLASS COUNTER WITH INCREMENT AND RESET METHODS
class Counter:
    count = 0       
    
    
    def increment(self): 
        self.count += 1  
    
    
    def reset(self):
        self.count = 0   


c1 = Counter()
c2 = Counter()


for c in "The quick brown fox jumps over the lazy dog.":
    c1.increment()
    

c2.increment()

print("We incremented c1", c1.count, "times.")
print("We incremented c2", c2.count, "times.")


c1.reset()
print("After reset, c1 count is at", c1.count)

# 2.WRITE A CLASS CALCULATOR WITH ALL BASIC ARITHMETIC OPERATIONS
class Calculator:
  def __init__(self):
    pass
  def add(self,x,y):
    return x+y
  def subtract(self,x,y):
    return x-y
  def multiply(self,x,y):
    return x*y
  def exponential(self,x,y):
    return x**y
  def divide(self,x,y):
    if y!=0:
      return x/y
    else:
      raise ValueError("cannot divide by zero")
calculator=Calculator()
x=int(input("enter the value of x:"))
y=int(input("enter the value of y:"))
result=calculator.add(x,y)
print(result)

result=calculator.subtract(x,y)
print(result)

result=calculator.multiply(x,y)
print(result)

result=calculator.exponential(x,y)
print(result)

result=calculator.divide(x,y)
print(result)

#3.Create a class Person with instance attributes name and age. Add a class attribute count that keeps track of the number of Person objects created.
class Person:
  count=0
  def __init__(self,name,age):
    self.name=name
    self.age=age
    Person.count+=1
person1=Person("Ironman",47)
person2=Person("Thor",35)
person3=Person("Hulk",52)

print(Person.count)

#4.Define a class BankAccount with instance attributes account_number and balance. Add a class attribute interest_rate that is set to 0.02. Add methods deposit and withdraw to modify the balance. Add a class method get_interest_rate that returns the value of the interest_rate attribute.
class BankAccount:
    
    def __init__(self,accountNumber, name, balance):
        self.accountNumber = accountNumber
        self.name = name
        self.balance = balance
        
    
    def Deposit(self , d ):
        self.balance = self.balance + d
    
    
    def Withdrawal(self , w):
        if(self.balance < w):
            print("impossible operation! Insufficient balance !")
        else:
            self.balance = self.balance - w
  
    def bankFees(self):
        self.balance = (95/100)*self.balance
        
    
    def display(self):
        print("Account Number : " , self.accountNumber)
        print("Account Name : " , self.name)
        print("Account Balance : " , self.balance , "/-")
        

newAccount = BankAccount(2178514584, "Black Widow" , 2700)

newAccount.Withdrawal(300)

newAccount.Deposit(200)

newAccount.display()

#5.Create a class Triangle with instance attributes side1, side2, and side3. Add a method is_equilateral that returns True if the triangle is equilateral (all sides are equal) and False otherwise. Create instances of Triangle and test the is_equilateral method.

class Triangle:
  side1=None
  side2=None
  side3=None

  def __init__(self,side1,side2,side3):
    self.side1=side1
    self.side2=side2
    self.side3=side3

  def is_equilateral(self):
    return self.side1==self.side2==self.side3

triangle1=Triangle(5,5,5)
print(triangle1.is_equilateral())

triangle2=Triangle(3,4,5)
print(triangle2.is_equilateral())

#6.Create a class called Rectangle that has two attributes width and height and two methods area() and perimeter(). Implement these methods to calculate the area and perimeter of a rectangle. Then, create another class called Square that inherits from Rectangle and has a single attribute side. Override the init() method to set the width and height to be equal to side. Implement the methods to calculate the area and perimeter of a square.
class Rectangle:
  def __init__(self,width,height):
    self.width=width
    self.height=height
  def area(self):
    return self.width*self.height
  def perimeter(self):
    return 2*(self.width+self.height)

class Square(Rectangle):
  def __init__(self,side):
    super().__init__(side,side)
    
rectangle=Rectangle(5,4)
print(rectangle.area())
print(rectangle.perimeter())

square=Square(5)
print(square.area())
print(square.perimeter())

#7.Create a class called Employee that has two attributes name and salary. Implement a method called calculate_bonus() that calculates the bonus of an employee based on their salary. Then, create two classes called Manager and Developer that inherit from Employee. Override the calculate_bonus() method for both classes to calculate the bonus based on a different percentage for each class.
class Employee:
  def __init__(self,name,salary):
    self.name=name
    self.salary=salary

  def calculate_bonus(self):
    return 0.1*self.salary

class Manager(Employee):
  def calculate_bonus(self):
    return 0.15*self.salary

class Developer(Employee):
  def calculate_bonus(self):
    return 0.12*self.salary

employee=Employee("Peter",5000)
print(employee.calculate_bonus())

manager=Manager("Parker",8000)
print(manager.calculate_bonus())

developer=Developer("Spidey",6000)
print(developer.calculate_bonus())

#1
#SALES PERSON
class Salesperson:
    def __init__(self, name):
        self.name = name
        self.record = {}
        self.value = 0.0

    def get_name(self):
        return self.name

    def buy_pro(self, product_name, market_price, quantity):
        if product_name in self.record:
            self.record[product_name][1] += quantity
        else:
            self.record[product_name] = [market_price, quantity]
        self.value -= market_price * quantity

    def sell_pro(self, product_name, market_price, quantity):
        if product_name in self.record:
            cost, stock = self.record[product_name]
            if stock >= quantity:
                self.record[product_name][1] -= quantity
                self.value += market_price * quantity
                if stock - quantity == 0:
                    del self.record[product_name]
            else:
                print(f"Not enough stock for {product_name}.")
        else:
            print(f"{product_name} is not in the record.")

    def get_record(self):
        return self.record

    def get_value(self):
        return self.value

salesperson = Salesperson("ram")


print(salesperson.get_name()) 


salesperson.buy_pro("Mjolnior", 1.0, 50)
salesperson.buy_pro("Storm Breaker", 0.8, 30)


salesperson.sell_pro("Mjolnior", 1.2, 20)
salesperson.sell_pro("Storm Breaker", 0.9, 40)  

print(salesperson.get_record())

print(salesperson.get_value())

#3
#DECK OF CARDS
import random

class Card:
    def __init__(self, suit, value):
        self.suit = suit
        self.value = value

    def __str__(self):
        return f"{self.value} of {self.suit}"

class Deck:
    def __init__(self):
        suits = ["Hearts", "Diamonds", "Clubs", "Spades"]
        values = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
        self.cards = [Card(suit, value) for suit in suits for value in values]

    def __str__(self):
        return f"Deck of {self.count()} cards"

    def count(self):
        return len(self.cards)

    def deal(self):
        if self.count() == 0:
            return None
        return self.cards.pop()

    def shuffle(self):
        if self.count() < 52:
            raise ValueError("Only full decks can be shuffled")
        random.shuffle(self.cards)

deck = Deck()


print(deck)

deck.shuffle()


print(deck)

card = deck.deal()
print(card)

print(deck.count())